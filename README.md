# CarCar

Team:

* Mike Miklius - Sales, manufacturer front-end (form, list), vehicle front-end (form, list)
* Ray Parker - Service

## Design

We will use boostrap to style our webpage.
We will create a SPA using react and react routes.
We will design 2 restful API microservices to create and store our Model Data.
We will make backend request with our React front-end

## Service microservice
My bounded context consists of the Service API and Service Poller.
I will create a (service) Apointment model that has the fields:
customer_name = CharField
date = DateField
time = TimeField
reason = TextField
finished = BooleanField
cancelled = BooleanField
technician = foreignKey(TechnicianVO)
automobile = foreignKey(AutomobileVO)

A Technician model with the fields:
name = charField (technician name)
employee_number = CharField (unique)

A Technician VO with the field:
name = charField (technician name)

An Automobile VO with the fields:
vin = CharField
import_href = CharField

The encoders to support the models are as follows:
DateEncoder - date
TimeEncoder - time in the format 12h hour : minutes am/pm
AutomobileVO Encoder - vin, import_href
TechnicianVOEncoder - name
TechnicianEncoder - name, employee_number
AppointmentDetailEncoder - customer_name,date, time, reason, finished, cancelled,       technician, automobile (encoders used: date, time, technician, automobileVO)
AppointmentListEncoder - customer_name,date, time, reason, id, finished, cancelled (encoders used: date, time) (extra data: automobile vin, technician name)

Views were made for:
-list appointments(GET,POST)
-show appointment(GET,PUT)
-list technicians(GET,POST)
-show history(GET)

4 URL paths were made for the views listed above.

The AutomobileVO will be updated via the Service_poller.py

I will then create the restful API to create,delete and edit Services.

Service History will use the BooleanFields to update the appointment to true if it was cancelled or finished rather than remove the appointment entirely. 

I used this API on our frontend to get data and dynamically update our webpage.

## Sales microservice

My bounded context was made up of the Sales API and the Sales poller. My design and process for building out my bounded context was as follows:
1. I installed my Sales_rest api in my sales_project/ settings file
2. I created a value object called AutomobileVO that grabbed an import href and a vin for each automobile created in the database.
3. I coded out my poller to populate my AutomobileVO with data. The poller made an internal request using the url "http://inventory-api:8000/api/automobiles/"
4. I built 3 models--Customer, Salesperson, and SalesRecord. Here are the fields for each model
        Customer: name, phone_number, and address
        Salesperson: name, employee_id
        SalesRecord: price and ForeignKeys to:AutomobileVO, Salesperson, and Customer
5. I built 4 models encoders to help manipulate my models (one for my VO and 1 for each model)
6. I built 4 view functions to deal with different data requests:
        A. list view for salespeople to handle GET/ POST requests
        B. list view for customers to handle GET/ POST requests
        C. list view for salesrecords to handle GET/ POST requests
        D. list view for 'available cars' to help populate one of my react pages
7. I built out url paths in my api_urls file (in sales_rest) and included those url paths in urls.py (sales_project dir)
