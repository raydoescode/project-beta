from common.json import ModelEncoder
from datetime import date, time
import json
from .models import Technician, Appointment, AutomobileVO


class DateEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, date):
            return o.isoformat()
        
        return json.JSONEncoder.default(self, o)


class TimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, time):
            return o.strftime('%I:%M %p')
        try:
            
            return json.JSONEncoder.default(self, o)
        except json.JSONDecodeError as e:
            return o


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
    ]


class TechnicianVOEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "date",
        "time",
        "reason",
        "id",
        "finished",
        "cancelled",
    ]
    encoders = {
        "date": DateEncoder(),
        "time": TimeEncoder(),
    }
    def get_extra_data(self,o):
        return {
            "automobile": o.automobile.vin,
            "technician": o.technician.name
        }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "date",
        "time",
        "reason",
        "finished",
        "cancelled",
        "technician",
        "automobile",
    ]
    encoders = {
        "date": DateEncoder(),
        "time": TimeEncoder(),
        "technician": TechnicianEncoder(),
        "automobile": AutomobileVOEncoder(),
    }
