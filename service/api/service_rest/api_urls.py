from django.urls import path
from .views import api_list_appointments, api_show_appointment, api_show_history, api_list_technicians


urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/", api_show_appointment, name="api_show_appointment"),
    path("history/", api_show_history, name="api_show_history"),
    path("technician/", api_list_technicians, name="api_list_technicans"),
]