# Generated by Django 4.0.3 on 2023-03-08 16:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_rename_description_appointment_reason_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='technician',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='appointment', to='service_rest.technician'),
        ),
        migrations.DeleteModel(
            name='TechnicianVO',
        ),
    ]
