from common.json import ModelEncoder

from .models import AutomobileVO, Customer, Salesperson, SalesRecord



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin"
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "phone_number",
        "address"
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["name", "employee_id"]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["price", "automobile", "salesperson", "customer"]
    encoders = {
        'automobile': AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder()
    }
