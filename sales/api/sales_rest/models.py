from django.db import models

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)



class Customer(models.Model):
    name = models.CharField(max_length=200)
    phone_number = models.BigIntegerField()
    address = models.CharField(max_length=200)

class Salesperson(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.IntegerField()


class SalesRecord(models.Model):
    price = models.PositiveIntegerField()

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salesrecords",
        on_delete=models.CASCADE,
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesrecords",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="salesrecords",
        on_delete=models.CASCADE,
    )
