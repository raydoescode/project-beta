import React, {useState, useEffect} from 'react';

function CustomerForm () {
    const [name, setName] = useState('');
    const [phone_number, setPhoneNumber] = useState('');
    const [address, setAddress] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.name = name
        data.phone_number = phone_number
        data.address = address

        const customerUrl = 'http://localhost:8090/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
            if (response.ok) {
                const newCustomer = await response.json();

                setName('');
                setPhoneNumber('');
                setAddress('');
            }
        }
        const handleNameChange = (event) => {
            const value = event.target.value;
            setName(value);
        }
        const handlePhoneNumberChange = (event) => {
            const value = event.target.value;
            setPhoneNumber(value);
        }
        const handleAddressChange = (event) => {
            const value = event.target.value;
            setAddress(value);
        }

  return (
    <>
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={phone_number} onChange={handlePhoneNumberChange} placeholder="phone_number" required type="number" name="phone_number" id="phone_number" className="form-control" />
              <label htmlFor="phone_number">Phone Number</label>
            </div>
            <div className="form-floating mb-3">
              <input value={address} onChange={handleAddressChange} placeholder="address" required type="text" name="address" id="address" className="form-control" />
              <label htmlFor="address">Address</label>
            </div>
            <button className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </>
  );

}
export default CustomerForm;
