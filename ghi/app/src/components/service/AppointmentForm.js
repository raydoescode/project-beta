import React, {useState, useEffect} from 'react';

function AppointmentForm () {
    const [technicians, setTechnicians] = useState([])
    const [formData, setFormData] = useState({
        vin: '',
        customer_name: '',
        date: '',
        time: '',
        employee_number: '',
        reason: ''
    })

    const getData = async () => {
        const url = `http://localhost:8080/api/technician/`;
        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            setTechnicians(data.Technician);
        }
    }

    useEffect(()=> {
        getData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const appointmentUrl = `http://localhost:8080/api/appointments/`;

        const body = {...formData, 
          'cancelled': false,
          'finished': false,
        };
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: '',
                customer_name: '',
                date: '',
                time: '',
                employee_number: '',
                reason: ''
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputId = e.target.id;
        setFormData({
            ...formData,
            [inputId]: value
        });
    }

    return ( <>
        <div className="row">
          <div className="offset-2 col-8">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Appointment</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                  <input value={formData.vin} onChange={handleFormChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                  <label htmlFor="vin">VIN</label>
                  </div>
                <div className="form-floating mb-3">
                  <input value={formData.customer_name} onChange={handleFormChange} placeholder="customer name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                  <label htmlFor="customer_name">Customer Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={formData.date} onChange={handleFormChange} placeholder="date" required type="date" name="date" id="date" className="form-control" />
                  <label htmlFor="date">Date</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={formData.time} onChange={handleFormChange} placeholder="time" required type="time" name="time" id="time" className="form-control" />
                  <label htmlFor="time">Time</label>
                </div>
                <div className="mb-3">
                  <select value={formData.employee_number} onChange={handleFormChange} required name="employee_number" id="employee_number" className="form-select">
                    <option key="technician">Assign a Technician</option>
                    {technicians.map(technician => {
                      return (
                        <option key={technician.employee_number} value={technician.employee_number}>
                          {technician.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <label htmlFor="reason">Reason</label>
                  <textarea value={formData.reason} onChange={handleFormChange} placeholder="reason" required type="textarea" name="reason" id="reason" className="form-control" rows={3}></textarea>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    </>
    );
 }

export default AppointmentForm;
