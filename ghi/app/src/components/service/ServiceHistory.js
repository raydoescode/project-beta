import React, {useState, useEffect} from 'react';

function ServiceHistory () {
    const [appointments, setAppointments] = useState([]);
    const [filterTerm, setFilterTerm] = useState("");

    const getData = async () => {
        const resp = await fetch(`http://localhost:8080/api/history/`);

        if (resp.ok) {
            const data = await resp.json();
            setAppointments(data);
        }
    }

    useEffect(()=> {
        getData();
    }, []);

    const handleFilterChange = (e) => {
        setFilterTerm(e.target.value);
    };

    return ( <>
        <div className="row">
          <div className="offset-2 col-8">
            <div className="shadow p-4 mt-4">
              <h1>Service History</h1>
              <hr />
              <input onChange={handleFilterChange} />
              <hr />
              <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Finished</th>
                        <th>cancelled</th>
                    </tr>
                </thead>

                <tbody>
                    {appointments
                    .filter((appointment) => appointment.automobile.includes(filterTerm))
                    .map(appointment => {
                            return (
                                <tr key={ `${appointment.automobile}-${appointment.date}-${appointment.time}-${Math.random() * 1000}` }>
                                    <td>{ appointment.automobile }</td>
                                    <td>{ appointment.customer_name}</td>
                                    <td>{ appointment.date }</td>
                                    <td>{ appointment.time }</td>
                                    <td>{ appointment.technician }</td>
                                    <td>{ appointment.reason }</td>
                                    <td>{ appointment.finished ? 'True' : 'False'}</td>
                                    <td>{ appointment.cancelled ? 'True' : 'False' }</td>
                                </tr>
                            );
                        })
                    }
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </>
    );
 }
    
export default ServiceHistory;