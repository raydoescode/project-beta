import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/manufacturer">Manufacturers</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/manufacturer_add">Manufacturer Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/vehicle">Vehicles</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/vehicle_add">Vehicle Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/automobile">Automobiles</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/automobile_add">Automobile Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/salesperson/add">Salesperson Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/salesperson/sales">Salesperson Sales</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/customer/add">Customer Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/sales">Sales List</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/sales_add">Sales Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/technician/add">Technician Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/appointment_add">Service Appointment Form</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/appointment">Service Appointment List</NavLink>
            </li>
            <li className="nav-link">
              <NavLink className="nav-link" aria-current="page" to="/history">Service History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
