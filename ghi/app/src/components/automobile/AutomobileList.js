import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'

const AutomobileList = () => {
    const [autos, setAutomobiles] = useState([])
    const [available, setAvailable] = useState([]);

    const getData = async () => {
        const resp = await fetch('http://localhost:8100/api/automobiles/')
        if (resp.ok) {
            const data = await resp.json()
            setAutomobiles(data.autos)
        }
    }

    useEffect(()=> {
        getData();
        fetchAvailable();
    }, [])

    const handleDelete = async (e) => {
        const url = `http://localhost:8100/api/automobiles/${e.target.id}`

        const fetchConfigs = {
            method: "Delete",
            headers:{
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)

        if (resp.ok) {
            const remove = autos.filter(automobile => automobile.vin !== e.target.id);
            setAutomobiles(remove)
        }

    }

    const fetchAvailable = async () => {
        const url = "http://localhost:8090/available/"

        const response = await fetch(url);

        if (response.ok) {
            const availableVins = [];
            const data = await response.json();
            for (let each of data.Available) {
                availableVins.push(each['id, vin'][1])
            }
            setAvailable(availableVins);

        }
    }

    return( <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>List Automobiles</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Vin</th>
                                <th>Color</th>
                                <th>Year</th>
                                <th>Model</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                autos.filter(automobile => available.includes(automobile.vin)).map(automobile => {
                                    return (
                                    <tr key={automobile.vin}>
                                        <td><Link to={`/automobiles/${automobile.vin}/`}>{ automobile.vin}</Link></td>
                                        <td>{ automobile.color }</td>
                                        <td>{ automobile.year }</td>
                                        <td>{ automobile.model.name }</td>
                                        <td><button onClick={handleDelete} id={automobile.vin} className="btn btn-danger">Delete</button></td>
                                    </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                    <Link to="/automobile_add"><button className='btn btn-primary'>Create an automobile</button></Link>
                </div>
            </div>
        </div>
    </>
    );
}

export default AutomobileList;
