import { useEffect, useState } from 'react';

function SalespersonSalesList() {
  const [Sales, setSalespersonSales] = useState([])
  const [seller, setSeller] = useState('')
  const [salespersonList, setSalespersonList] = useState([])


  const getSales = async () => {
    const response = await fetch('http://localhost:8090/sales/');
    if (response.ok) {
      const data = await response.json();
      setSalespersonSales(data.Sales)
    }
  }

  const getSalespeople = async () => {
    const url = "http://localhost:8090/salespeople/"

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setSalespersonList(data.Salespeople);
    }
}

  useEffect(()=>{
    getSales()
  }, [])

  useEffect(()=>{
    getSalespeople()
  }, [])

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSeller(value);
}

  return (
    <>

    <h1>
      Sales Summary
    </h1>
    <div className="mb-3">
        <select value="employees" onChange={handleSalespersonChange} required name="employees" id="employees" className="form-select">
        {/* THIS CODE WAS BEFORE REQUIRED NAME AND AFTER VALUE= THING          onChange={handleSalespersonChange} */}
        <option>Choose a Salesperson</option>
        {salespersonList.map(salesperson => {
            return (
            <option key={salesperson.name} value={salesperson.name}>
                {salesperson.name}
            </option>
            );
        })}
        </select>
    </div>
    <hr></hr>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperson</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {Sales.filter(sale => sale.salesperson.name === seller).map(sale => {
          return (
            <tr key={sale.vin}>
              <td>{ sale.salesperson.name }</td>
              <td>{ sale.customer.name }</td>
              <td>{ sale.automobile.vin }</td>
              <td>${ sale.price }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default SalespersonSalesList;
