import React, {useState, useEffect} from 'react';

function SalespersonForm () {
    const [name, setName] = useState('');
    const [employee_id, setEmployeeId] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.name = name
        data.employee_id = employee_id

        const salespersonUrl = 'http://localhost:8090/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig);
            if (response.ok) {
                setName('');
                setEmployeeId('');
            }
        }
        const handleNameChange = (event) => {
            const value = event.target.value;
            setName(value);
        }
        const handleEmployeeIdChange = (event) => {
            const value = event.target.value;
            setEmployeeId(value);
        }

  return (
    <>
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Sales Person</h1>
          <form onSubmit={handleSubmit} id="create-manufacturer-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={employee_id} onChange={handleEmployeeIdChange} placeholder="employee_id" required type="number" name="employee_id" id="employee_id" className="form-control" />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </>
  );

}
export default SalespersonForm;
