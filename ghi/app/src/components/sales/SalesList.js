import { useEffect, useState } from 'react';

function SalesList() {
  const [Sales, setSales] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/sales/');
    if (response.ok) {
      const data = await response.json();
      setSales(data.Sales)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <>

    <h1>
      Sales Summary
    </h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperson</th>
          <th>Salesperson ID</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {Sales.map(sale => {
          return (
            <tr key={sale.href}>
              <td>{ sale.salesperson.name }</td>
              <td>{ sale.salesperson.employee_id }</td>
              <td>{ sale.customer.name }</td>
              <td>{ sale.automobile.vin }</td>
              <td>${ sale.price }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default SalesList;
