import React, {useState, useEffect} from 'react';

function VehicleForm () {
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = picture_url;
        data.manufacturer_id = Number(manufacturer);

        const vehicleUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(vehicleUrl, fetchConfig);
            if (response.ok) {

                setName('');
                setPictureUrl('');
                setManufacturer('');
            }
        }

        const fetchData = async () => {
            const url = "http://localhost:8100/api/manufacturers/"

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            }
        }

        const handleNameChange = (event) => {
            const value = event.target.value;
            setName(value);
        }

        const handlePictureUrlChange = (event) => {
            const value = event.target.value;
            setPictureUrl(value);
        }

        const handleManufacturerChange = (event) => {
            const value = event.target.value;
            setManufacturer(value);
        }

        useEffect(() => {
            fetchData();
        }, []);

  return (
    <>
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Vehicle</h1>
          <form onSubmit={handleSubmit} id="create-vehicle-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture_url} onChange={handlePictureUrlChange} placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="mb-3">
              <select value={manufacturer} onChange={handleManufacturerChange} required id="manufacturer" className="form-select" name="manufacturer">
              <option value ="">Manufacturer</option>
              {manufacturers.map(manufacturer => {
                return (
                    <option value = {manufacturer.id} key = {manufacturer.id}>
                        {manufacturer.name}
                    </option>
                )
              })}
              </select>
            </div>
            <button className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </>
  );

}
export default VehicleForm;
