import { useEffect, useState } from 'react';

function VehicleList() {
  const [models, setVehicle] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      setVehicle(data.models)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  const handleDelete = async (e) => {
    const url = `http://localhost:8100/api/models/${e.target.id}/`

    const fetchConfigs = {
      method: "Delete",
      headers:{
        "Content-Type": "application/json"
      }
    }

    const resp = await fetch(url, fetchConfigs)
    const data = await resp.json()


    setVehicle(models.filter(model => String(model.id) !== e.target.id))

  }

  return (
    <>

    <h1>
      Vehicle Models
    </h1>
    <table className="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {models.map(vehicle => {
          return (
            <tr key={ vehicle.href }>
              <td>{ vehicle.name }</td>
              <td>{ vehicle.manufacturer.name }</td>
              <td><img src={ vehicle.picture_url } alt="Car" className="img-fluid"/></td>
              <td><button onClick={handleDelete} id={vehicle.id} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>

    </>
  );
}

export default VehicleList;
